﻿/**
 * jQuery tilapsmoothscroll is a jquery plugin in javascript to render smootscrool 
 * in page easily user jQuery selector. Compatible in most modern browser
 * Copyright (C) 2013 Julien LA VINH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
 * of the Software, and to permit persons to whom the Software is furnished to do 
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 */

/**
 * Basic usage on a link with class "myLink" :
 * jQuery('.myLink').tilapSmoothScroll('fast');
 *
 * You can run it on a container, thus all the link inside with an
 * anchor in the same page will get smoothscroll
 */
(function(jQuery){
    $.fn.tilapSmoothScroll = function(scrolSpeed, topExtraIndent){
        if('undefined'==typeof(scrolSpeed)) {
            var scrolSpeed = 'slow';
        }
        if('undefined'==typeof(topExtraIndent)) {
            var topExtraIndent = 0;
        }

        // Loop on every item inside
        return this.each(function(){
            jQuery(this).click(function(){
                var applyTheSmoothscrool = false;
                var the_hash = jQuery(this).attr("href");
                var regex = new RegExp("\#(.*)","gi");
                if(the_hash.match("\#(.+)")) {

                    the_hash = the_hash.replace(regex,"$1");

                    if(jQuery("#"+the_hash).length>0) {
                        var the_element = "#" + the_hash;
                        applyTheSmoothscrool = true;
                    }
                    else if(jQuery("a[name=" + the_hash + "]").length>0) {
                        var the_element = "a[name=" + the_hash + "]";
                        applyTheSmoothscrool = true;
                    }

                    if(applyTheSmoothscrool) {
                        // Container selection compatibility for webkit
                        if ('webkitRequestAnimationFrame' in window) {
                            var container = 'body';
                        }
                        else {
                            var container = 'html';
                        }
                        // Do the scroll
                        jQuery(container).animate({
                            scrollTop : jQuery(the_element).offset().top + topExtraIndent
                        }, scrolSpeed,
                            // Put tabindex to 0
                            function(){
                                jQuery(the_element).attr('tabindex','0').removeAttr('tabindex');
                            }
                        );
                        return false;
                    }
                }
            });
        });
    };
})(jQuery)
